import qualified Phonebook.Interface.HTTP as HTTP

import Phonebook.Utils

main = connectAndRun HTTP.main
