module Phonebook.Storage (
    create, read, update, delete,
    ContactId, Name, Phone
  ) where

import Prelude hiding (read)
import Database.HDBC
import qualified Data.ByteString.Char8 as BS

type ContactId = Integer
type Name = String
type Phone = String

create :: IConnection a => Name -> Phone -> a -> IO Bool
create name phone conn =
  withTransaction conn (create' name phone)

create' name phone conn = do
  changed <- run conn query [SqlString name, SqlString phone]
  return $ changed == 1
  where
    query = "insert into phonebook (name, phone, last_changed)" ++
            " values (?, ?, now())"

read :: IConnection a => a -> IO [(ContactId, Name, Phone)]
read conn = do
  rslt <- quickQuery' conn query []
  return $ map unpack rslt
  where
    query = "select id, name, phone from phonebook order by id"
    unpack [SqlInteger cid, SqlByteString name, SqlByteString phone] =
      (cid, BS.unpack name, BS.unpack phone)
    unpack x = error $ "Unexpected result: " ++ show x

update :: IConnection a => ContactId -> Name -> Phone -> a -> IO Bool
update cid name phone conn =
  withTransaction conn (update' cid name phone)

update' cid name phone conn = do
  changed <- run conn query
                 [SqlString name, SqlString phone, SqlInteger cid]
  return $ changed == 1
  where
    query = "update phonebook set name = ?, phone = ?," ++
            " last_changed = now() where id = ?"

delete :: IConnection a => ContactId -> a -> IO Bool
delete cid conn =
  withTransaction conn (delete' cid)

delete' cid conn = do
  changed <- run conn "delete from phonebook where id = ?"
                 [SqlInteger cid]
  return $ changed == 1
