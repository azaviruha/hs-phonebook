module Phonebook.Utils (
    connectAndRun
  ) where

import Database.HDBC
import Database.HDBC.PostgreSQL
import Data.Configurator
import Control.Exception
import Data.Time.Clock
import Data.Pool

connectAndRun fun = do
  (cs, s, t, c) <- getConnectParams
  pool <- createPool (connectPostgreSQL cs) disconnect s t c
  fun pool

getConnectParams :: IO (String, Int, NominalDiffTime, Int)
getConnectParams = do
  conf <- loadConfig
  let dbconf = subconfig "database" conf
  [name, user, pass]           <- mapM (\n -> require dbconf n :: IO String)
                                       ["name", "user", "pass"]
  [port, stripes, connections] <- mapM (\(k, d) -> lookupDefault d dbconf k :: IO Int)
                                       [("port", 5432),("stripes",1),("connections",5)]
  host <- lookupDefault "localhost" dbconf "host" :: IO String
  let cs = "host=" ++ host ++ " port=" ++ show port ++ " dbname=" ++ name ++
           " user=" ++ user ++ " password=" ++ pass
  timeout <- lookupDefault 60 dbconf "timeout" :: IO Integer
  return (cs, stripes, fromInteger timeout, connections)
  where
    fname = ".phonebookrc"
    loadConfig =
      catch (load [Required $ "$(HOME)/" ++ fname])
            (\e ->
              do mapM_ putStrLn $ ["Failed to open ~/" ++ fname, ""] ++
                                  usage fname
                 throw (e :: IOException))

usage :: String -> [String]
usage fname =
  ["Configure PostgreSQL server and create ~/" ++ fname ++ " like this:"
  ,"database {"
  ,"  name        = \"database_name\""
  ,"  user        = \"user\""
  ,"  pass        = \"qwerty\""
  ,"  host        = \"localhost\" # optional"
  ,"  port        = 5432        # optional"
  ,"  stripes     = 1           # optional"
  ,"  timeout     = 60          # optional"
  ,"  connections = 5           # optional"
  ,"}"
  ,""
  ,"Database should be inited with following query:"
  ,"CREATE TABLE phonebook (id SERIAL PRIMARY KEY, name varchar(64), phone varchar(64), last_changed timestamp);"
  ,""]

