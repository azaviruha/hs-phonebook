module Phonebook.Interface.HTTP (main) where

import Prelude hiding (read)
import qualified Web.Scotty as Sc
import qualified Phonebook.Storage as St
import qualified Data.Map as M
import Network.HTTP.Types.Status
import Control.Monad.IO.Class
import Data.Aeson
import Data.Pool

main pool = Sc.scotty 8080 $ do
  Sc.post   "/api/v1.0/contacts"      (create pool)
  Sc.get    "/api/v1.0/contacts"      (read pool)
  Sc.put    "/api/v1.0/contacts/:cid" (update pool)
  Sc.delete "/api/v1.0/contacts/:cid" (delete pool)

create pool = do
  body    <- Sc.jsonData :: Sc.ActionM (M.Map String String)
  success <- withNamePhone body (\n p -> withResource pool $ St.create n p)
  Sc.status $ boolToHttpCode success

read pool = do
  contacts <- liftIO $ withResource pool St.read
  Sc.json $ map toObj contacts
  where
    toObj :: (St.ContactId, St.Name, St.Phone) -> Value
    toObj (cid, name, phone) =
      object [ "contactId" .= cid
             , "name"      .= name
             , "phone"     .= phone
             ]

update pool = do
  cid     <- Sc.param "cid" :: Sc.ActionM Integer
  body    <- Sc.jsonData :: Sc.ActionM (M.Map String String)
  success <- withNamePhone body (\n p -> withResource pool $ St.update cid n p)
  Sc.status $ boolToHttpCode success

delete pool = do
  cid <- Sc.param "cid" :: Sc.ActionM Integer
  success <- liftIO $ withResource pool $ St.delete cid
  Sc.status $ boolToHttpCode success

withNamePhone m action =
  case extractNamePhone m of
    Nothing -> return False
    Just (name, phone) -> liftIO $ action name phone

extractNamePhone :: M.Map String String -> Maybe (String, String)
extractNamePhone m = do
  name  <- M.lookup "name"  m
  phone <- M.lookup "phone" m
  return (name, phone)

boolToHttpCode success =
  if success then noContent204
             else badRequest400
