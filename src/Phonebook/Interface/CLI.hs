module Phonebook.Interface.CLI (main) where

import qualified Phonebook.Storage as S
import System.IO
import Control.Monad (when)
import Data.Pool

main pool = do
  putStrLn "Enter a command ('h' for help, 'q' for quit)."
  mainLoop pool

mainLoop pool = do
  putStr "phonebook> "
  hFlush stdout
  line <- getLine
  continue <- withResource pool $ \conn -> processCmd conn (words line)
  when continue $ mainLoop pool

processCmd c ("c":name:phone:_)     = createContact name phone c
processCmd c ("r":_)                = readContacts c
processCmd c ("u":cid:name:phone:_) = updateContact (read cid :: S.ContactId) name phone c
processCmd c ("d":cid:_)            = deleteContact (read cid :: S.ContactId) c
processCmd _ ("q":_)                = quit
processCmd _ ("h":_)                = showHelp
processCmd _ _                      = showError

createContact name phone conn =
  S.create name phone conn >>= showRslt

readContacts conn =
  S.read conn >>= mapM_ print >> return True

updateContact cid name phone conn =
  S.update cid name phone conn >>= showRslt

deleteContact cid conn =
  S.delete cid conn >>= showRslt

showRslt :: Bool -> IO Bool
showRslt True = putStrLn "Ok" >> return True
showRslt False = putStrLn "Err" >> return True

quit :: IO Bool
quit = do
  putStrLn "Goodbye!"
  return False

showHelp :: IO Bool
showHelp = do
  mapM_ putStrLn help
  return True

showError :: IO Bool
showError = do
  putStrLn "Unexpected command, enter 'h' for help"
  return True

help :: [String]
help =
  ["h               - help"
  ,"q               - quit"
  ,"c name phone    - create contact"
  ,"r               - read all contacts"
  ,"u id name phone - update contact with specified id"
  ,"d id            - delete contact with specified id"
  ]
